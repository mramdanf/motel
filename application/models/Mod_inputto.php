<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_inputto extends CI_Model {

	private $to_table = "target_operasi";

	function __construct()
	{
		parent::__construct();
	}

	function modify($data = NULL, $id = NULL)
	{
		if ($id == NULL) 
		{
			return $this->db->insert_batch($this->to_table, $data);
		
		} else
		{
			return $this->db->update($this->to_table, $data, array('id'=>$id));
		}
	}

	function data_to()
	{
		//return $this->db->get($this->to_table)->result();
		$result = $this->db
			     ->select('target_operasi.*, user.name')
			     ->from('target_operasi')
			     ->join('user', 'target_operasi.id_user = user.id')
			     ->get()->result();

		return $result;
	}

	function delete($id = NULL)
	{
		return $this->db->delete($this->to_table, array('id'=>$id));
	}

	function target_operasi($id_user = NULL)
	{
		$this->db->where('id_user', $id_user);
		$this->db->where('checked', 0);
		return $this->db->get($this->to_table)->result();
	}

	function target_operasi_by_id($id = "")
	{
		$res = $this->db->get_where($this->to_table, array('id'=>$id))->row();

		$res->tanggal = $this->utility->dateForPicker($res->tanggal);

		return $res;
	}

	function to_perteam_inaday($filter = array())
	{
		/*SELECT
			id_user, tanggal, DAY(tanggal) AS hari, 
			MONTH(tanggal) AS bulan, YEAR(tanggal) AS tahun,
			COUNT(DATE(tanggal)) AS jml_to
		FROM 
			target_operasi
		GROUP BY 
			hari, id_user
		HAVING
			id_user = 2 
			AND YEAR(tanggal) = 2017 
			AND MONTH(tanggal) = 4	*/

		$result = $this->db
				     ->select('id_user, tanggal, DAY(tanggal) AS hari, 
						COUNT(DATE(tanggal)) AS jml_to')
				     ->from('target_operasi')
				     ->group_by('hari, id_user')
				     ->having('id_user', $filter['id_user'])
				     ->having('YEAR(tanggal)', $filter['tahun'])
				     ->having('MONTH(tanggal)', $filter['bulan'])
				     ->get()->result();

		return $result;

	}

}

/* End of file Mod_inputto.php */
/* Location: ./application/models/Mod_inputto.php */