<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_pemeriksaan extends CI_Model {

	private $pemeriksaan_tbl = "pemeriksaan";

	function __construct()	
	{
		parent::__construct();
	}

	function pemeriksaan($filter = array())
	{
		$this->db
				->select('pemeriksaan.*, target_operasi.id_pelanggan, user.*')
				->from('pemeriksaan')
				->join('target_operasi','target_operasi.id = pemeriksaan.id_target_operasi')
				->join('user', 'target_operasi.id_user = user.id');
		
		if (!empty($filter['id_user']))
			$this->db->where('target_operasi.id_user', $filter['id_user']);

		if (!empty($filter['tanggal']))
			$this->db->where('pemeriksaan.tanggal', $filter['tanggal']);

		$result = $this->db->get()->result();

		return $result;

	}

	function delete($id = NULL)
	{
		return $this->db->delete($this->pemeriksaan_tbl, array('id'=>$id));
	}

	function modify($data = NULL)
	{
		date_default_timezone_set("Asia/Jakarta");
		
		$data_photo = $this->upload($data);

		$data['photo'] = $data_photo['photo'];
		$data['photo2'] = $data_photo['photo2'];

		if (!isset($data['id']) || $data['id'] == "") 
		{
			$date = DateTime::createFromFormat('d/m/Y', $data['tanggal']);
			$data['tanggal'] = $date->format('Y-m-d');
			if ($this->db->insert($this->pemeriksaan_tbl, $data))
			{
				$data_to['checked'] = 1;
				return $this->db->update('target_operasi', $data_to, array('id'=>$data['id_target_operasi']));
			}
		
		} else
		{
			$id = $data['id'];
			unset($data['id']);
			return $this->db->update($this->pemeriksaan_tbl, $data, array('id'=>$id));
		}
	}

	function get_all_pemeriksaan()
	{
		$sql = "SELECT pemeriksaan.*, target_operasi.id_pelanggan, user.name\n"
		    . "	FROM pemeriksaan\n"
		    . "	INNER JOIN\n"
		    . "	target_operasi\n"
		    . "	ON target_operasi.id = pemeriksaan.id_target_operasi\n"
		    . " INNER JOIN\n"
		    . " user\n"
		    . " ON user.id = target_operasi.id_user";


		return $this->db->query($sql)->result();
	}

	function upload($data = NULL)
	{
		$config['upload_path'] = './assets/img/pemeriksaan';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		
		$this->load->library('upload', $config);
		
		// photo 1
		if ( ! $this->upload->do_upload("img_pemeriksaan"))
		{
			$error = array(
	                'error' => $this->upload->display_errors(),
	                'file_name' => $this->upload->data()['file_name'],
	                'file_type' => $this->upload->data()['file_type'],
	        );
	        $this->utility->plog($error);
		}
		else
		{
			$succes_data = array('upload_data' => $this->upload->data());
			
			$photo = $this->upload->data()['file_name'];
		}

		// photo2
		if ( ! $this->upload->do_upload("img_pemeriksaan2"))
		{
			$error = array(
	                'error' => $this->upload->display_errors(),
	                'file_name' => $this->upload->data()['file_name'],
	                'file_type' => $this->upload->data()['file_type'],
	        );
	        $this->utility->plog($error);
		}
		else
		{
			$succes_data = array('upload_data' => $this->upload->data());
			
			$photo2 = $this->upload->data()['file_name'];
		}

		$data_photo['photo'] = $photo;
		$data_photo['photo2'] = $photo2;

		return $data_photo;
	}

	function pemeriksaan_with_month()
	{
		return $this->db
		             ->select('pemeriksaan.*, MONTH(pemeriksaan.tanggal) AS bulan_pemeriksaan')
		             ->from('pemeriksaan')
		             ->get()->result();
	}

	function jml_pemeriksaan_perteam_inaday($filter = NULL)
	{
		// yang bikin fungsi ini kompleks adalah golongan != 'None'
		$result = $this->db
		               ->select('target_operasi.id_user, pemeriksaan.id_target_operasi, 
							pemeriksaan.tanggal, DAY(pemeriksaan.tanggal) AS hari,
							MONTH(pemeriksaan.tanggal) AS bulan, YEAR(pemeriksaan.tanggal) AS tahun,
						    COUNT(pemeriksaan.tanggal) AS jml')
		               ->from('pemeriksaan')
		               ->join('target_operasi', 'target_operasi.id = pemeriksaan.id_target_operasi')
		               ->group_by('hari, target_operasi.id_user, golongan')
		               ->having('target_operasi.id_user', $filter['id_user'])
		               ->having('MONTH(pemeriksaan.tanggal)', $filter['bulan'])
		               ->having('YEAR(pemeriksaan.tanggal)', $filter['tahun'])
		               ->having('golongan !=', 'None')
		               ->get()->result_array();
		
		// return for controller
		$new_result = array();

		if (sizeof($result) > 0) 
		{
			// tampung tanggal pertama dari result
			$new_result[0]['tanggal'] = $result[0]['tanggal'];

			// temporary var for tanggal pertama dari $result
			$tmp_date = $result[0]['tanggal'];

			// cari semua tanggal yang ada di $result tanpa perulangan, dan
			// masukan hasilnya ke $new_resutl
			$i=1;
			foreach ($result as $row) 
			{
				if ($tmp_date != $row['tanggal'])
				{
					$tmp_date = $row['tanggal'];
					$new_result[$i++]['tanggal'] = $tmp_date;
				}
			}

			// setelah didapatkan tanggal yang unik, jumlahkan banyaknya pemeriksaan
			// bersesuaian dengan tanggal yang ada pada $new_result,
			// kemudian tambahkan banyaknya pemeriksaan di array $new_result.
			foreach ($new_result as $key => $item) 
			{
				$jml_pemeriksaan = 0;

				foreach ($result as $row) 
				{
					if ($item['tanggal'] == $row['tanggal'])
					{
						$jml_pemeriksaan += $row['jml'];
						$new_result[$key]['hari'] = $row['hari'];
					}
				}

				$new_result[$key]['jml'] = $jml_pemeriksaan;
					
			}
		}		

		return $new_result;

	}

	function pprint($data)
	{
		echo "<pre>";
		print_r($data);
	}

	/*
		@desc: jumlah temuan per golongan dalam satu hari
	*/
	function jml_peremiksaan_gol_inaday($filter = array())
	{
		/*SELECT
			tanggal, golongan, COUNT(golongan) AS jml_golongan
		FROM
			pemeriksaan
		GROUP BY
			tanggal, golongan
		HAVING 
			tanggal = 'tgl'*/

		$result = $this->db
		               ->select('tanggal, golongan, COUNT(golongan) AS jml_pemeriksaan')
		               ->from('pemeriksaan')
		               ->group_by('tanggal, golongan')
		               ->having('tanggal', $filter['tanggal'])
		               ->get()->result();

		
		foreach ($result as $row) 
		{
			// Populate $result with color, this for pie chart color
			$row->color = '#'.$this->random_color();
		}
		
		return $result;

	}

	// ================ helper functions ================


	// helper function for generate hex color for pie chart
	function random_color_part() 
	{
	    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	function random_color() 
	{
	    return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

}

/* End of file Mod_pemeriksaan.php */
/* Location: ./application/models/Mod_pemeriksaan.php */