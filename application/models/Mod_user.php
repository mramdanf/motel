<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_user extends CI_Model {

	private $user_tbl = "user";

	function __construct()
	{
		parent::__construct();
	}

	function check_login($data = NULL)
	{
		if ($data != NULL) 
		{
			return $this->db->get_where($this->user_tbl, 
				array('username'=>$data['username'], 'password'=>$data['password'])
			)->row();
		}
	}

	function users()
	{
		$this->db->where('hak_akses !=', "1"); // akun admin tidak ditampilkan
		$this->db->where('hak_akses !=', "2"); // akun asman tidak ditampilkan
		return $this->db->get($this->user_tbl)->result();
	}

	function modify($data = NULL)
	{
		if ($data['id'] == NULL) 
		{
			return $this->db->insert($this->user_tbl, $data);
		
		} else 
		{
			$id = $data['id'];
			unset($data['id']);
			return $this->db->update($this->user_tbl, $data, array('id'=>$id));
		}
	}

	function delete($id = NULL)
	{
		return $this->db->delete($this->user_tbl, array('id'=>$id));
	}

	function user($id = NULL)
	{
		return $this->db->get_where($this->user_tbl, array('id'=>$id))->row();
	}

}

/* End of file User.php */
/* Location: ./application/models/User.php */