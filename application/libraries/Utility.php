<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility
{
	private $CI;

	const PICKUP = "Pickup";
	const SHIFT_1 = "Shift Satu";
	const SHIFT_2 = "Shift Dua";
	const SUCCESS = "success";
	const FAILED = "failed";
	const ALERT_MSG = "alert_msg";
	const ADM_SES = "adm_ses";
	const STATUS = 'status';
	const MSG = 'msg';

	const HTTP_OK = 200;
	const HTTP_UNAUTHORIZED = 401;
	const HTTP_BAD_REQUEST = 400;
	const HTTP_INTERNAL_SERVER_ERROR = 500;
	const HTTP_CREATED = 201;
	const HTTP_NO_CONTENT = 204;
	const HTTP_NOT_FOUND = 404;

	function __construct()
	{
		$this->CI =& get_instance();
		date_default_timezone_set("Asia/Jakarta");
	}

	/*
		@desc: prety print
	*/
	function pprint($data)
	{
		echo "<pre>";
		print_r($data);
	}

	/*
		print array to error log
		@params: array $data array to print
	*/
	function plog($data)
	{
		log_message('error', print_r($data, TRUE));
	}

	/*
		set_flash_data for alert
		flag: indicate type of alert
	*/
	function set_flash($flag = "", $msg)
	{
		if($flag != "")
		{
			if($flag == self::SUCCESS)
			{
				$this->CI->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>$msg,
				        'type'=>'alert-success'
				    )
				);
			} else
			{
				$this->CI->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>$msg,
				        'type'=>'alert-danger'
				    )
				);
			}
		}
	}

	function s_flash_v2($flag = "", $msg = "")
	{
		if($flag != "")
		{
			$type = ($flag == self::SUCCESS) ? "success":"error";

			$alert = array(
				'title' => $flag,
				'msg' => $msg,
				'type' => $type
				);

			$this->CI->session->set_flashdata(
				    'alert', 
				    $alert
				);
		}
	}

	function dateSimpleForm($data = "")
	{
		return date("Y-m-d", strtotime($data));
	}

	function dateForPicker($data = "")
	{
		return date("m/d/Y", strtotime($data));
	}

	public function credential()
	{
		if (!isset($_SERVER['PHP_AUTH_USER'])) 
		{
		    header('WWW-Authenticate: Basic realm="Motel Realm"');
		    header('HTTP/1.0 401 Unauthorized');

			$message = [
				self::STATUS => FALSE,
				self::MSG => 'Login gagal, username atau password salah.'
			];

			$this->print_json($message, utility::HTTP_UNAUTHORIZED);
		    exit;

		} else 
		{
		    $username = $_SERVER['PHP_AUTH_USER'];
		    $password =  $_SERVER['PHP_AUTH_PW'];

		    $result = $this->checkUser($username, $password);
			
			if (!empty($result)) 
			{
				return $result;

			}else
			{
				$message = [
					self::STATUS => FALSE,
					self::MSG => 'Login gagal, username atau password salah.'
				];
				$this->print_json($message, utility::HTTP_UNAUTHORIZED);
				exit;
			}
		}
	}

	public function checkUser($username, $password)
	{
		$this->CI->load->database();

		$this->CI->db->where('username', $username);
		$this->CI->db->where('password', $password);

		$query = $this->CI->db->get('user');

		return $query->row();
	}

	public function print_json($data, $http_code)
	{
		$this->CI->output
	      ->set_status_header($http_code)
	      ->set_content_type('application/json', 'utf-8')
	      ->set_output(json_encode($data, JSON_PRETTY_PRINT))
	      ->_display();
	    exit;
	}

	public function link_for_grafik($hak_akses = 1)
	{
		$this->CI->load->model('Mod_user');

		$curr_year = date("Y");
		$curr_month = (int) date("m");
		$curr_day = (int) date("d");
		$curr_tgl_grafik2 = date("Y_m_d");
		$default_team = $this->CI->Mod_user->users()[0]->id;

		if ($hak_akses == 1)
		{
			$link_grafik1 = 
				site_url("admin/grafik/grafik1?tahun={$curr_year}&bulan={$curr_month}&id_user={$default_team}");

			$link_grafik2 = 
				site_url("admin/grafik/grafik2?tanggal={$curr_tgl_grafik2}");
		
		} else
		{
			$link_grafik1 = 
				site_url("asman/grafik/grafik1?tahun={$curr_year}&bulan={$curr_month}&id_user={$default_team}");

			$link_grafik2 = 
				site_url("asman/grafik/grafik2?tanggal={$curr_tgl_grafik2}");

		}
		
		$link_grafik['link_grafik1'] = $link_grafik1;
		$link_grafik['link_grafik2'] = $link_grafik2;

		return $link_grafik;
	}

}