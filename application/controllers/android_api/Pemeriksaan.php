<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->utility->credential();
		$this->load->model('Mod_pemeriksaan');
	}

	public function index()
	{
		$id_user = $this->input->get('id_user');

		$result = $this->Mod_pemeriksaan->pemeriksaan($id_user);

		if ($result) 
		{
			$response = array(
				utility::STATUS => TRUE,
				"pemeriksaan" => $result
				);
			$this->utility->print_json($response, utility::HTTP_OK);

		} else 
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::MSG => "No data found."
				);
			$this->utility->print_json($response, utility::HTTP_NOT_FOUND);
		}
	}

	function delete()
	{
		$id = $this->input->post('id');
		if ($this->Mod_pemeriksaan->delete($id)) 
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::MSG => "Data berhasil dihapus."
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		
		} else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::MSG => "Terjadi eror, data gagal dihapus."
				);
			$this->utility->print_json($response, utility::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	function modify()
	{
		$data = $this->input->post();
		
		if ($this->Mod_pemeriksaan->modify($data)) 
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::MSG => "Data berhasil disimpan."
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		
		} else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::MSG => "Terjadi eror, data gagal disimpan."
				);
			$this->utility->print_json($response, utility::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

}

/* End of file Pemeriksaan.php */
/* Location: ./application/controllers/Pemeriksaan.php */