<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo "string";
	}

	function login()
	{
		$res = $this->utility->credential();
		
		if ($res != NULL) 
		{
			$response = array(
				utility::STATUS => TRUE,
				"user" => $res
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		}
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/android_api/auth.php */