<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target_operasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->utility->credential();
		$this->load->model('Mod_inputto');
	}

	public function index()
	{
		$id_user = $this->input->get('id_user');
		
		$result = $this->Mod_inputto->target_operasi($id_user);

		if ($result) 
		{
			$response = array(
				utility::STATUS => TRUE,
				"operations" => $result
				);
			$this->utility->print_json($response, utility::HTTP_OK);

		} else 
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::MSG => "No data found."
				);
			$this->utility->print_json($response, utility::HTTP_NOT_FOUND);
		}
	}

}

/* End of file Target_operasi.php */
/* Location: ./application/controllers/android_api/Target_operasi.php */