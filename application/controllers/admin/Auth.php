<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function login()
	{
		$curr_user = $this->session->userdata(utility::ADM_SES);

		if ($curr_user != NULL) 
		{
			if ($curr_user->hak_akses == 1)
				redirect('admin/input_to');
			else
				redirect('admin/pemeriksaan');
		
		} else 
		{
			$this->load->view('admin/login');
		}
		
	}

	function do_check_login()
	{
		$this->load->model('Mod_user');

		$data = $this->input->post();

		$res = $this->Mod_user->check_login($data);

		if ($res) 
		{
			if ($res->hak_akses == "1") 
			{				
				$this->session->set_userdata(utility::ADM_SES, $res);
				redirect('admin/input_to');
			
			} else if($res->hak_akses == 2)
			{
				$this->session->set_userdata(utility::ADM_SES, $res);
				redirect('admin/pemeriksaan');

			} else {

				$this->utility->set_flash(utility::FAILED, "Login gagal, username atau password salah.");
				redirect("admin/auth/login");

			}

		} else {

			$this->utility->set_flash(utility::FAILED, "Login gagal, username atau password salah.");
			redirect("admin/auth/login");
		}
	}

	function logout()
	{
		$this->session->unset_userdata(utility::ADM_SES);
		redirect("admin/auth/login");
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/admin/Auth.php */