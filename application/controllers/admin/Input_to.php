<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_to extends CI_Controller {

	private $title = "Input TO";

	function __construct()
	{
		parent::__construct();
		$curr_user = $this->session->userdata(utility::ADM_SES);
		if ($curr_user->hak_akses != 1 || $curr_user == NULL) 
		{
			redirect("admin/auth/login");
		}
		$this->load->model('Mod_inputto');
	}

	public function index()
	{
		$link_grafik = $this->utility->link_for_grafik();

		$data = array(
			"page_view" => "admin/input_to",
			"title" => $this->title,
			"teams" => $this->Mod_user->users(),
			"data_to" => $this->Mod_inputto->data_to(),
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);
		$this->load->view('admin/template', $data);
	}

	function form($id = NULL)
	{
		$this->load->model('Mod_user');
		$data = array(
			"page_view" => "admin/input_to_form",
			"title" => "Edit TO"
			);

		if ($id != NULL) 
		{
			$data['teams'] = $this->Mod_user->users();
			$data['target_operasi'] = $this->Mod_inputto->target_operasi_by_id($id);
		}

		$this->load->view('admin/template', $data);
	}

	function modify()
	{
		$data = $this->input->post();
		if (!isset($data['id'])) 
		{
			$config['upload_path'] = './assets/files/';
			$config['allowed_types'] = 'xls|xlsx';
			$config['max_size']  = '50000';
			$config['encrypt_name']  = TRUE;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload("excel_file"))
			{
				$error = array('error' => $this->upload->display_errors());
				$this->utility->plog($error);
				$this->utility->s_flash_v2(utility::FAILED, "Terjadi error, data gagal disimpan. " 
					. $error['error']);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());

				$data_for_db = array(
					"excel_name" => $this->upload->data()['file_name'],
					"tanggal" => $this->utility->dateSimpleForm($this->input->post('tanggal')),
					"id_user" => $this->input->post('team')
					);

				if ($this->insert_excel_to($data_for_db))
				{
					$this->utility->s_flash_v2(utility::SUCCESS, "Data berhasil disimpan");
				
				} else
				{
					$this->utility->s_flash_v2(utility::FAILED, "Terjadi error, data gagal disimpan");
				}
				
			}

		} else 
		{
			$id = $data['id'];
			$data['tanggal'] = $this->utility->dateSimpleForm($data['tanggal']);
			unset($data['id']);

			if ($this->Mod_inputto->modify($data, $id)) 
			{
				$this->utility->s_flash_v2(utility::SUCCESS, "Data berhasil disimpan");
			
			} else
			{
				$this->utility->s_flash_v2(utility::FAILED, "Terjadi error, data gagal disimpan");
			}
		}

		redirect('admin/input_to');
	}

	function insert_excel_to($data_for_db = NULL)
	{
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$inputFileName = 'assets/files/' . $data_for_db['excel_name'];

		try 
		{
		    $inputFileType = iofactory::identify($inputFileName);
		    $objReader = iofactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);

		    
		} catch(Exception $e) 
		{
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$arr_data = array("");

		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) 
		{
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

		    //header will/should be in row 1 only. of course this can be modified to suit your need.
		    if ($row == 1) 
		    {
		        $header[$row][$column] = $data_value;

		    } else 
		    {
		    	if ($data_value != NULL) $arr_data[$row][$column] = $data_value;		        
		    }
		}
		
		$arr_data = array_filter($arr_data);
		$arr_data = array_map(function($arr_data) {
			    return array(
			        'id_pelanggan' => $arr_data['B'],
			        'kd_unit' => $arr_data['C'],
			        'nama' => $arr_data['D'],
			        'alamat' => $arr_data['E'],
			        'daya' => $arr_data['F'],
			        'tarif' => $arr_data['G'],
			        'no_gardu' => $arr_data['H'],
			        'kd_jtr' => $arr_data['I'],
			        'merek' => $arr_data['J'],
			        'seri' => $arr_data['K'],
			        'latitude' => $arr_data['L'],		        
			        'longitude' => $arr_data['M'],		        
			        'ket_dlpd' => $arr_data['N'],		        
			        
			    );
			}, $arr_data);

		foreach ($arr_data as $key => $value) 
		{
			$arr_data[$key]['tanggal'] = $data_for_db['tanggal'];
			$arr_data[$key]['id_user'] = $data_for_db['id_user'];
			$arr_data[$key]['checked'] = 0;
		}

		return $this->Mod_inputto->modify($arr_data);
	}

	function delete()
	{
		$id = $this->input->post('id');
		if ($id != NULL) 
		{
			if ($this->Mod_inputto->delete($id)) 
			{
				$this->utility->s_flash_v2(utility::SUCCESS, "Data berhasil dihapus.");
			
			} else
			{
				$this->utility->s_flash_v2(utility::FAILED, "Terjadi error, data gagal dihapus.");
			}
		}
		redirect('admin/input_to');
	}

}

/* End of file Input_to.php */
/* Location: ./application/controllers/admin/Input_to.php */