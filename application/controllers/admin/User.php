<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	private $title = "Manjemen User";

	function __construct()
	{
		parent::__construct();
		$curr_user = $this->session->userdata(utility::ADM_SES);
		if ($curr_user->hak_akses != 1 || $curr_user == NULL) 
		{
			redirect("admin/auth/login");
		}
		$this->load->model('Mod_user');
	}

	public function index()
	{
		$link_grafik = $this->utility->link_for_grafik();
		
		$data = array(
			"page_view" => "admin/user",
			"title" => $this->title,
			"users" => $this->Mod_user->users(),
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);
		$this->load->view('admin/template', $data);
	}

	function form($id = NULL)
	{
		$link_grafik = $this->utility->link_for_grafik();

		$data = array(
			"page_view" => "admin/user_form",
			"title" => $this->title,
			"sub_title" => "Tambah User",
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);

		if ($id != NULL) 
		{
			$data['user'] = $this->Mod_user->user($id);
		}
		
		$this->load->view('admin/template', $data);
	}

	function modify()
	{
		$data = $this->input->post();
		
		if ($this->Mod_user->modify($data)) 
		{
			$alert = array(
				'title' => 'Success',
				'msg' => "Data berhasil disimpan.",
				'type' => 'success'
				);
			$this->session->set_flashdata('alert', $alert);
		
		} else 
		{
			$alert = array(
				'title' => 'Failed',
				'msg' => "Data gagal disimpan.",
				'type' => 'error'
				);
			$this->session->set_flashdata('alert', $alert);
		}
		redirect('admin/user');
	}

	function delete()
	{
		$id = $this->input->post('id');

		log_message('error','id: ' . $id);

		if ($this->Mod_user->delete($id)) 
		{
			$alert = array(
				'title' => 'Success',
				'msg' => "Data berhasil dihapus.",
				'type' => 'success'
				);
			$this->session->set_flashdata('alert', $alert);
		
		} else
		{
			$alert = array(
				'title' => 'Failed',
				'msg' => "Terjadi error, data gagal dihapus.",
				'type' => 'error'
				);
			$this->session->set_flashdata('alert', $alert);
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/admin/User.php */