<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller {

	private $title = "Grafik";

	private $bulan_id = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

	function __construct()
	{
		parent::__construct();
		$curr_user = $this->session->userdata(utility::ADM_SES);
		if ($curr_user == NULL) 
		{
			redirect("admin/auth/login");
		}
		$this->load->model('Mod_pemeriksaan');
		$this->load->model('Mod_inputto');
		$this->load->model('Mod_user');
	}

	public function index()
	{
		
	}


	function grafik1()
	{
		$link_grafik = $this->utility->link_for_grafik();

		$data = array(
			"page_view" => "admin/grafik",
			"title" => $this->title,
			"teams" => $this->Mod_user->users(),
			"nama_bulan" => $this->bulan_id,
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);
		$this->load->view('admin/template', $data);
	}

	function grafik2()
	{
		$link_grafik = $this->utility->link_for_grafik();

		$data = array(
			"page_view" => "admin/grafik2",
			"title" => $this->title,
			"teams" => $this->Mod_user->users(),
			"nama_bulan" => $this->bulan_id,
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);
		$this->load->view('admin/template', $data);
	}



	/*
		@desc: Grafik kinerja team perhari (dalam suatu tahun dan bulan)
		@get: id_user, tahun, bulan
	*/
	function json_first_grafik()
	{
		$filter = $this->input->get();

		// variable untuk wrapper semua data yang akan ditampilkan di grafik
		$data_grafik = array();

		if (!empty($filter)) 
		{
			// banyaknya pemeriksaan yang diassign ke satu team tertentu in a day
			$to_perteam_inaday = $this->Mod_inputto->to_perteam_inaday($filter);

			// jml pemeriksaan yg telah dilakukan oleh satu team tertentu in a day
			$pemeriksaan_inaday 
				= $this->Mod_pemeriksaan->jml_pemeriksaan_perteam_inaday($filter);

			//$this->utility->plog($to_perteam_inaday);

			

			foreach ($to_perteam_inaday as $to) 
			{
				// item dari data_grafik
				$data_grafik_item = array();

				foreach ($pemeriksaan_inaday as $pemeriksaan) 
				{
					// check berapa jml yang telah diperiksa perhari
					
					// kondisi ketika jml TO yang diperiksa di suatu hari tertentu > 0
					// dengan kata lain ada satu hari di pemeriksaan yang 
					// mana hari itu sama dengan hari di data TO.
					if ($to->hari == $pemeriksaan['hari']) 
					{
						$data_grafik_item['hari'] = date("d M Y", strtotime($to->tanggal));
						$data_grafik_item['presentasi'] = number_format(($pemeriksaan['jml']/$to->jml_to * 100), 0);
						$data_grafik_item['jml_pemeriksaan'] = $pemeriksaan['jml'];
						break;
					
					} else
					{
						// Kondisi tidak ada TO yang diperiksa, jumlah pemeriksaan pasti 0
						$data_grafik_item['hari'] = date("d M Y", strtotime($to->tanggal));
						$data_grafik_item['jml_pemeriksaan'] = 0;
						$data_grafik_item['presentasi'] = 0;
					}

					// jml_to tidak digunakan oleh grafik, 
					// hanya untuk mepermudah tracing saja
					$data_grafik_item['jml_to'] = $to->jml_to;
				}				

				if (!empty($data_grafik_item))  
					array_push($data_grafik, $data_grafik_item);
			}
		
		} 	

		$this->output
	        ->set_status_header(200)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($data_grafik, JSON_PRETTY_PRINT));
	}

	function json_second_grafik()
	{
		$filter = $this->input->get();

		// Change tanggal in from undescore to strip easy query in DB
		$filter['tanggal'] = str_replace('_', '-', $filter['tanggal']);

		// jumlah temuan per golongan dalam satu hari
		$jml_temuan = $this->Mod_pemeriksaan->jml_peremiksaan_gol_inaday($filter);

		$this->output
	        ->set_status_header(200)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($jml_temuan, JSON_PRETTY_PRINT));
	}

	function coba()
	{
		$input = array("a" => "green", "red", "b" => "green", "blue", "red");
		$result = array_unique($input);

		echo "<pre>";
		print_r($input);
	}


}

/* End of file Grafik.php */
/* Location: ./application/controllers/admin/Grafik.php */