<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends CI_Controller {

	private $title = "Pemeriksaan";

	function __construct()
	{
		parent::__construct();
		$curr_user = $this->session->userdata(utility::ADM_SES);
		if ($curr_user == NULL) 
		{
			redirect("admin/auth/login");
		}
		$this->load->model('Mod_pemeriksaan');
	}

	public function index()
	{
		$link_grafik = $this->utility->link_for_grafik();

		$data = array(
			"title" => $this->title,
			"teams" => $this->Mod_user->users(),
			"pemeriksaan" => $this->Mod_pemeriksaan->get_all_pemeriksaan(),
			"page_view" => "admin/pemeriksaan",
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);
		$this->load->view('admin/template', $data);
	}

	function filter()
	{
		$filter = $this->input->get();
		$pemeriksaan = $this->Mod_pemeriksaan->pemeriksaan($filter);

		$link_grafik = $this->utility->link_for_grafik();

		$data = array(
			"title" => $this->title,
			"teams" => $this->Mod_user->users(),
			"pemeriksaan" => $pemeriksaan,
			"page_view" => "admin/pemeriksaan",
			"link_grafik1" => $link_grafik['link_grafik1'],
			"link_grafik2" => $link_grafik['link_grafik2']
			);
		$this->load->view('admin/template', $data);
	}

	function export_excel()
	{
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$query = $this->Mod_pemeriksaan->get_all_pemeriksaan();

		if ($query) 
		{
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Pemeriksaan")->setDescription("none");

			$objPHPExcel->setActiveSheetIndex(0);

			
			// header of table naming (for excel)
			$fields = array('tanggal', 'id_pelanggan', 'stand_kwh', 'error_kwh', 'keterangan', 'name');
	
			$col = 0;
			foreach ($fields as $field) 
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, ucfirst($field));
				$col++;
			}

			// Fetching the table data
			$row = 2;
			foreach ($query as $data) 
			{
				$col = 0;
				foreach ($fields as $field) 
				{
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}

				$row++;
			}

			$objPHPExcel->setActiveSheetIndex(0);

			// coloring cel A1 to F1
			$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '33B833')
			        )
			    )
			);
			

			$objWriter = iofactory::createWriter($objPHPExcel, 'Excel5');

			header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment;filename="Pemeriksaan_'.date('dMy').'.xls"');
	        header('Cache-Control: max-age=0');
	 
	        $objWriter->save('php://output');
	    }
	}

}

/* End of file Pemeriksaan.php */
/* Location: ./application/controllers/admin/Pemeriksaan.php */