<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter Data</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="filter-pemeriksaan" class="form-horizontal form-label-left" action="<?=site_url('admin/pemeriksaan/filter')?>">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" class="form-control has-feedback-left" id="tgl_filter_pemeriksaan" aria-describedby="inputSuccess2Status4" name="tanggal">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <span id="inputSuccess2Status4" class="sr-only">(success)</span>
            </div>
          </div>
          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Team</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select class="form-control" id="id_user_pemeriksaan" name="id_user">
                <option>Pilih team</option>
                <?php foreach ($teams as $team) { ?>
                <option value="<?=$team->id?>"><?=$team->name?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="reset">Reset</button>
              <button type="submit" class="btn btn-success">Filter</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data Pemeriksaan</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content table-responsive">
        <a href="<?=site_url('admin/pemeriksaan/export_excel')?>" class="btn btn-success" style="margin-bottom: 10px;">Export excel</a>
        <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Tgl. Pemeriksaan</th>
              <th>ID. Pelanggan</th>
              <th>Error KWH</th>
              <th>Kode Pelanggaran</th>
              <th>Keterangan</th>
              <th>Photo</th>
              <th>Photo 2</th>
              <th>Team Pemeriksa</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; ?>
            <?php foreach ($pemeriksaan as $key => $value) { ?>
            <tr>
              <th scope="row"><?=$no++?></th>
              <td><?=$value->tanggal?></td>
              <td><?=$value->id_pelanggan?></td>
              <td><?=$value->error_kwh?></td>
              <td><?=$value->golongan?></td>
              <td><?=$value->keterangan?></td>
              <td><a class="fancybox" href="<?=site_url('./assets/img/pemeriksaan'.'/'.$value->photo)?>">Klik untuk melihat gambar</a></td>
              <td><a class="fancybox" href="<?=site_url('./assets/img/pemeriksaan'.'/'.$value->photo2)?>">Klik untuk melihat gambar</a></td>
              <td><?=$value->name?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>