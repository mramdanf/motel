<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data User</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content table-responsive">
        <a class="btn btn-primary" style="margin-bottom: 20px;" href="<?=site_url('admin/user/form')?>">Tambah User</a>
        <table id="user_table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Password</th>
              <th>Nomor HP</th>
              <th>Hak Akses</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1; ?>
            <?php foreach ($users as $user) { ?>
            <tr>
              <th scope="row"><?=$no++?></th>
              <td><?=$user->name?></td>
              <td><?=$user->username?></td>
              <td><?=$user->password?></td>
              <td><?=$user->no_hp?></td>
              <td><?=$user->hak_akses?></td>
              <td><a href="javascript:void(null)" id="<?=$user->id?>" title="Delete user" class="btn btn-danger glyphicon glyphicon-trash delete_user"></a>
              <a href="<?=site_url('admin/user/form'.'/'.$user->id)?>" title="Edit user" class="btn btn-success glyphicon glyphicon-pencil"></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    var baseUrl = "<?=site_url('')?>";
    <?php $alert = $this->session->flashdata('alert'); ?>
    <?php if($alert != NULL) { ?>
      new PNotify({
          title: "<?=$alert['title']?>",
          text: "<?=$alert['msg']?>",
          type: "<?=$alert['type']?>",
          styling: 'bootstrap3'
      });
    <?php } ?>
    
    $("#user_table").on('click', '.delete_user', function() {
      var data_id = this.id;
      console.log("id: " + data_id);
      if (!confirm("Anda yakin?")) {
        return;
      }

      $.post(baseUrl + 'admin/user/delete', {id: this.id}, function() {
        window.location.replace(baseUrl + 'admin/user');
      });
    });

  });
</script>