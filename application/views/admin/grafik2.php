<style>
  /* rmd */
.chart-legend ul li { 
  list-style:none; display: inline-block;
  margin-right: 10px;
}
.chart-legend li span{
    display: inline-block;
    width: 12px;
    height: 12px;
    margin-right: 5px;
 }
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter Grafik</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="x_content">
        <form class="form-horizontal form-label-left" method="get" id="filter_grafik2">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" class="form-control has-feedback-left" id="tgl_grafik2" aria-describedby="inputSuccess2Status4" name="tanggal">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <span id="inputSuccess2Status4" class="sr-only">(success)</span>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="reset">Reset</button>
              <button type="submit" class="btn btn-success">Filter</button>
            </div>
          </div>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-4">
    <div class="x_panel">
      <div class="x_title">
        <h2>Kumulatif Jumlah</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
        <small id="ket_tgl_grafik2"><i class="fa fa-calendar"></i> Tanggal</small>
      </div>
      <div class="x_content">
        <canvas id="grafikDua"></canvas>
      </div>
      <div class="x_content">
        <div id="js-legend" class="chart-legend"></div>
      </div>
    </div>
  </div>
</div>
