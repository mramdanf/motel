<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AmPel | <?=$title?></title>

    <!-- Bootstrap -->
    <link href="<?=site_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=site_url('assets/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?=site_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css')?>" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?=site_url('/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">
    <!-- PNotify -->
    <link href="<?=site_url('assets/vendors/pnotify/dist/pnotify.css')?>" rel="stylesheet">
    <link href="<?=site_url('assets/vendors/pnotify/dist/pnotify.buttons.css')?>" rel="stylesheet">
    <link href="<?=site_url('assets/vendors/pnotify/dist/pnotify.nonblock.css')?>" rel="stylesheet">
    <!-- Fancybox -->
    <link rel="stylesheet" href="<?=site_url('assets/vendors/fancyBox/source/jquery.fancybox.css?v=2.1.6')?>" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?=site_url('assets/vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')?>" type="text/css" media="screen"/>

    <!-- Custom Theme Style -->
    <link href="<?=site_url('assets/vendors/gantelella/css/custom.min.css')?>" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?=site_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
  </head>

  <?php 
    $curr_user = $this->session->userdata(utility::ADM_SES);
    $curr_login = ($curr_user->hak_akses==1) ? 'Admin':'Asman'; 
  ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-bolt"></i> <span>Monitoring P2TL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?=site_url('assets/img/admin_img.jpg')?>" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?=$curr_login?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu</h3>
                <ul class="nav side-menu"> 
                  <li><a><i class="fa fa-bar-chart-o"></i> Grafik <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?=$link_grafik1?>">Persentasi Pemeriksaan <br> Per Team</a></li>
                      <li><a href="<?=$link_grafik2?>">Temuan Pemeriksaan</a></li>
                    </ul>
                  </li>
                  <li><a href="<?=site_url('admin/pemeriksaan')?>"><i class="fa fa-history"></i> Pemeriksaan </a></li>
                  <?php  
                    if ($curr_user->hak_akses == 1) {
                  ?>                
                  <li><a href="<?=site_url('admin/input_to')?>"><i class="fa fa-pencil-square-o"></i> Input TO </a></li>                  
                  <li><a href="<?=site_url('admin/user')?>"><i class="fa fa-users"></i> Manajemen User </a></li>
                  <?php } ?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?=site_url('assets/img/admin_img.jpg')?>" alt=""><?=$curr_login?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?=site_url('admin/auth/logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?=$title?></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <?php $this->load->view($page_view); ?>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Monitoring P2TL
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    
    <!-- Bootstrap -->
    <script src="<?=site_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- Chart.js -->
    <script src="<?=site_url('assets/vendors/Chart.js/dist/Chart.min.js')?>"></script>
    <!-- Datatables -->
    <script src="<?=site_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?=site_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
    <!-- PNotify -->
    <script src="<?=site_url('assets/vendors/pnotify/dist/pnotify.js')?>"></script>
    <script src="<?=site_url('assets/vendors/pnotify/dist/pnotify.buttons.js')?>"></script>
    <script src="<?=site_url('assets/vendors/pnotify/dist/pnotify.nonblock.js')?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?=site_url('assets/vendors/moment/min/moment.min.js')?>"></script>
    <script src="<?=site_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
    <!-- Fancybox -->
    <script type="text/javascript" src="<?=site_url('assets/vendors/fancyBox/source/jquery.fancybox.pack.js?v=2.1.6')?>"></script>
    <script type="text/javascript" src="<?=site_url('assets/vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?=site_url('assets/vendors/gantelella/js/custom.js')?>"></script>
	
  </body>
</html>
