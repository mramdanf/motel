<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Form Edit TO</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left" method="post" action="<?=site_url('admin/input_to/modify')?>">

            <!-- hidden input for helper -->
            <input type="hidden" name="id" value="<?=$target_operasi->id?>">

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Team 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="id_user">
                  <option>Pilih team</option>
                  <?php 
                    $sel = "";
                    foreach ($teams as $team) 
                    {
                      $sel = ($team->id == $target_operasi->id_user) ? "selected":"";
                      echo "<option {$sel} value='".$team->id."'>{$team->name}</option>";
                    }

                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ID Pelanggan 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="id_pelanggan" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->id_pelanggan?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Unit 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="kd_unit" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->kd_unit?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="nama" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->nama?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="alamat" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->alamat?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Daya 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="daya" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->daya?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tarif 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="tarif" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->tarif?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No. Gardu 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="no_gardu" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->no_gardu?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode JTR 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="kd_jtr" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->kd_jtr?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Merek 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="merek" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->merek?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Seri 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="seri" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->seri?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control has-feedback-left" id="single_cal4" aria-describedby="inputSuccess2Status4" name="tanggal" value="<?=$target_operasi->tanggal?>">
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                <span id="inputSuccess2Status4" class="sr-only">(success)</span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Longitude 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="longitude" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->longitude?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Latitude 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="latitude" class="form-control col-md-7 col-xs-12" value="<?=$target_operasi->latitude?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ket. DLPD 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea name="ket_dlpd" class="form-control" rows="5"><?=$target_operasi->ket_dlpd?></textarea>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="button" onclick="history.back()">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>