<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="<?=site_url('admin/input_to/modify')?>" method="post">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" class="form-control has-feedback-left" id="single_cal4" aria-describedby="inputSuccess2Status4" name="tanggal">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <span id="inputSuccess2Status4" class="sr-only">(success)</span>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Input TO <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="file" name="excel_file" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Team</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select class="form-control" name="team">
                <option>Pilih team</option>
                <?php foreach ($teams as $team) { ?>
                <option value="<?=$team->id?>"><?=$team->name?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button class="btn btn-primary" type="reset">Reset</button>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Data TO</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content table-responsive">
        <table id="datatable" class="table table-striped table-bordered table_to">
          <thead>
            <tr>
              <th>#</th>
              <th>Team</th>
              <th>ID Pelanggan</th>
              <th>Kode Unit</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Daya</th>
              <th>Tarif</th>
              <th>No. Gardu</th>
              <th>Kode JTR</th>
              <th>Merk Meter</th>
              <th>No. Seri</th>
              <th>Latitude</th>
              <th>Longitude</th>
              <th>Ket. DLPD</th>
              <th>Tanggal</th>
              <th>Pemeriksaan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; ?>
            <?php foreach ($data_to as $to) { ?>
            <tr>
              <th scope="row"><?=$no++;?></th>
              <td><?=$to->name?></td>
              <td><?=$to->id_pelanggan?></td>              
              <td><?=$to->kd_unit?></td>              
              <td><?=$to->nama?></td>
              <td><?=$to->alamat?></td>
              <td><?=$to->daya?></td>
              <td><?=$to->tarif?></td>
              <td><?=$to->no_gardu?></td>
              <td><?=$to->kd_jtr?></td>
              <td><?=$to->merek?></td>
              <td><?=$to->seri?></td>
              <td><?=$to->latitude?></td>
              <td><?=$to->longitude?></td>
              <td><?=$to->ket_dlpd?></td>
              <td><?=$to->tanggal?></td>
              <td><?=($to->checked)?'<label class="label label-success"><span class="glyphicon glyphicon-ok"></span></label>':''?></td>
              <td><a href="javascript:void(null)" title="Hapus TO" id="<?=$to->id?>" class="btn btn-danger glyphicon glyphicon-trash delete_to"></a><a href="<?=site_url('admin/input_to/form'.'/'.$to->id)?>" title="Edit TO" class="btn btn-success glyphicon glyphicon-pencil"></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    var baseUrl = "<?=site_url('')?>";
    <?php $alert = $this->session->flashdata('alert'); ?>
    <?php if($alert != NULL) { ?>
      new PNotify({
          title: "<?=$alert['title']?>",
          text: "<?=$alert['msg']?>",
          type: "<?=$alert['type']?>",
          styling: 'bootstrap3'
      });
    <?php } ?>

    $(".table_to").on('click', '.delete_to', function() {
      var data_id = this.id;
      console.log('id: ' + data_id);
      if (!confirm("Anda yakin?")) {
        return;
      }

      $.post(baseUrl + '/admin/input_to/delete', {id: data_id}, 
        function() {
        window.location.replace(baseUrl + '/admin/input_to');
      });
    });

  });
</script>