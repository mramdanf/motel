<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AmPel | Login </title>

    <!-- Bootstrap -->
    <link href="<?=site_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=site_url('assets/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=site_url('assets/vendors/gantelella/css/custom.min.css')?>" rel="stylesheet">
  </head>

  <body class="login">
    <div>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php $alert = $this->session->flashdata('alert'); ?>
            <?php if ($alert != NULL) { ?>
            <div class="alert <?=$alert['type']?> alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button>
              <?=$alert['msg']?>
            </div>
            <?php } ?>

            <form action="<?=site_url('admin/auth/do_check_login')?>" method="post">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" name="username" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-bolt"></i> Monitoring P2TL</h1>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
  <!-- jQuery -->
  <script src="<?=site_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
  <!-- Bootstrap -->
  <script src="<?=site_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
</html>
