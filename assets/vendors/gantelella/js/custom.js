/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args); 
                timeout = null; 
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100); 
        };
    };

    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

	
	
// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
var setContentHeight = function () {
	// reset height
	$RIGHT_COL.css('min-height', $(window).height());

	var bodyHeight = $BODY.outerHeight(),
		footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
		leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
		contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

	// normalize content
	contentHeight -= $NAV_MENU.height() + footerHeight;

	$RIGHT_COL.css('min-height', contentHeight);
};

$SIDEBAR_MENU.find('a').on('click', function(ev) {
  console.log('clicked - sidebar_menu');
    var $li = $(this).parent();

    if ($li.is('.active')) {
        $li.removeClass('active active-sm');
        $('ul:first', $li).slideUp(function() {
            setContentHeight();
        });
    } else {
        // prevent closing menu if we are on child menu
        if (!$li.parent().is('.child_menu')) {
            $SIDEBAR_MENU.find('li').removeClass('active active-sm');
            $SIDEBAR_MENU.find('li ul').slideUp();
        }else
        {
			if ( $BODY.is( ".nav-sm" ) )
			{
				$SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
				$SIDEBAR_MENU.find( "li ul" ).slideUp();
			}
		}
        $li.addClass('active');

        $('ul:first', $li).slideDown(function() {
            setContentHeight();
        });
    }
});

// toggle small or large menu 
$MENU_TOGGLE.on('click', function() {
		console.log('clicked - menu toggle');
		
		if ($BODY.hasClass('nav-md')) {
			$SIDEBAR_MENU.find('li.active ul').hide();
			$SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
		} else {
			$SIDEBAR_MENU.find('li.active-sm ul').show();
			$SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
		}

	$BODY.toggleClass('nav-md nav-sm');

	setContentHeight();
});

	// check active menu
	$SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

	$SIDEBAR_MENU.find('a').filter(function () {
		return this.href == CURRENT_URL;
	}).parent('li').addClass('current-page').parents('ul').slideDown(function() {
		setContentHeight();
	}).parent().addClass('active');

	// recompute content when resizing
	$(window).smartresize(function(){  
		setContentHeight();
	});

	setContentHeight();

	// fixed sidebar
	if ($.fn.mCustomScrollbar) {
		$('.menu_fixed').mCustomScrollbar({
			autoHideScrollbar: true,
			theme: 'minimal',
			mouseWheel:{ preventDefault: true }
		});
	}
};
// /Sidebar

	var randNum = function() {
	  return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
	};


// Panel toolbox
$(document).ready(function() {
    $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');
        
        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200); 
            $BOX_PANEL.css('height', 'auto');  
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar

// Switchery
$(document).ready(function() {
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A'
            });
        });
    }
});
// /Switchery


// iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

// Table
$('table input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var checkState = '';

$('.bulk_action input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    checkState = 'all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    checkState = 'none';
    countChecked();
});

function countChecked() {
    if (checkState === 'all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
    }
    if (checkState === 'none') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
    }

    var checkCount = $(".bulk_action input[name='table_records']:checked").length;

    if (checkCount) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(checkCount + ' Records Selected');
    } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
    }
}



// Accordion
$(document).ready(function() {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// NProgress
if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}

	
  	//hover and retain popover when on popover content
    var originalLeave = $.fn.popover.Constructor.prototype.leave;
    $.fn.popover.Constructor.prototype.leave = function(obj) {
      var self = obj instanceof this.constructor ?
        obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
      var container, timeout;

      originalLeave.call(this, obj);

      if (obj.currentTarget) {
        container = $(obj.currentTarget).siblings('.popover');
        timeout = self.timeout;
        container.one('mouseenter', function() {
          //We entered the actual popover – call off the dogs
          clearTimeout(timeout);
          //Let's monitor popover content instead
          container.one('mouseleave', function() {
            $.fn.popover.Constructor.prototype.leave.call(self, self);
          });
        });
      }
    };

    $('body').popover({
      selector: '[data-popover]',
      trigger: 'click hover',
      delay: {
        show: 50,
        hide: 400
      }
    });


	function gd(year, month, day) {
		return new Date(year, month - 1, day).getTime();
	}	  
	   
	/* SELECT2 */
  
	function init_select2() {
		 
		if( typeof (select2) === 'undefined'){ return; }
		console.log('init_toolbox');
		 
		$(".select2_single").select2({
		  placeholder: "Select a state",
		  allowClear: true
		});
		$(".select2_group").select2({});
		$(".select2_multiple").select2({
		  maximumSelectionLength: 4,
		  placeholder: "With Max Selection limit 4",
		  allowClear: true
		});
		
	};

   	/* DATERANGEPICKER */
   
	function init_daterangepicker() {

		if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
		console.log('init_daterangepicker');
	
		var cb = function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		};

		var optionSet1 = {
		  startDate: moment().subtract(29, 'days'),
		  endDate: moment(),
		  minDate: '01/01/2012',
		  maxDate: '12/31/2015',
		  dateLimit: {
			days: 60
		  },
		  showDropdowns: true,
		  showWeekNumbers: true,
		  timePicker: false,
		  timePickerIncrement: 1,
		  timePicker12Hour: true,
		  ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		  },
		  opens: 'left',
		  buttonClasses: ['btn btn-default'],
		  applyClass: 'btn-small btn-primary',
		  cancelClass: 'btn-small',
		  format: 'MM/DD/YYYY',
		  separator: ' to ',
		  locale: {
			applyLabel: 'Submit',
			cancelLabel: 'Clear',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Custom',
			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			firstDay: 1
		  }
		};
		
		$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		$('#reportrange').daterangepicker(optionSet1, cb);
		$('#reportrange').on('show.daterangepicker', function() {
		  console.log("show event fired");
		});
		$('#reportrange').on('hide.daterangepicker', function() {
		  console.log("hide event fired");
		});
		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		  console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
		});
		$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
		  console.log("cancel event fired");
		});
		$('#options1').click(function() {
		  $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
		});
		$('#options2').click(function() {
		  $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
		});
		$('#destroy').click(function() {
		  $('#reportrange').data('daterangepicker').remove();
		});

	}
	   
   	function init_daterangepicker_right() {
      
		if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
		console.log('init_daterangepicker_right');
  
		var cb = function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		  $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		};

		var optionSet1 = {
		  startDate: moment().subtract(29, 'days'),
		  endDate: moment(),
		  minDate: '01/01/2012',
		  maxDate: '12/31/2020',
		  dateLimit: {
			days: 60
		  },
		  showDropdowns: true,
		  showWeekNumbers: true,
		  timePicker: false,
		  timePickerIncrement: 1,
		  timePicker12Hour: true,
		  ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		  },
		  opens: 'right',
		  buttonClasses: ['btn btn-default'],
		  applyClass: 'btn-small btn-primary',
		  cancelClass: 'btn-small',
		  format: 'MM/DD/YYYY',
		  separator: ' to ',
		  locale: {
			applyLabel: 'Submit',
			cancelLabel: 'Clear',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Custom',
			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			firstDay: 1
		  }
		};

		$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

		$('#reportrange_right').daterangepicker(optionSet1, cb);

		$('#reportrange_right').on('show.daterangepicker', function() {
		  console.log("show event fired");
		});
		$('#reportrange_right').on('hide.daterangepicker', function() {
		  console.log("hide event fired");
		});
		$('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
		  console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
		});
		$('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
		  console.log("cancel event fired");
		});

		$('#options1').click(function() {
		  $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
		});

		$('#options2').click(function() {
		  $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
		});

		$('#destroy').click(function() {
		  $('#reportrange_right').data('daterangepicker').remove();
		});

   	}
   
    function init_daterangepicker_single_call() {
      
		if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
		console.log('init_daterangepicker_single_call');
	   
		$('#single_cal1').daterangepicker({
		  singleDatePicker: true,
		  singleClasses: "picker_1"
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#single_cal2').daterangepicker({
		  singleDatePicker: true,
		  singleClasses: "picker_2"
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#single_cal3').daterangepicker({
		  singleDatePicker: true,
		  singleClasses: "picker_3"
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#single_cal4').daterangepicker({
		  singleDatePicker: true,
		  singleClasses: "picker_4"
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#tgl_grafik2').daterangepicker({ 
		  singleDatePicker: true,
		  singleClasses: "picker_4",
		  locale: {
		      format: 'DD/MM/YYYY'
		    }
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});
		$('#tgl_filter_pemeriksaan').daterangepicker({ 
		  singleDatePicker: true,
		  singleClasses: "picker_4",
		  locale: {
		      format: 'YYYY-MM-DD'
		    }
		}, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});


	}
	
	 
	function init_daterangepicker_reservation() {
      
		if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
		console.log('init_daterangepicker_reservation');
	 
		$('#reservation').daterangepicker(null, function(start, end, label) {
		  console.log(start.toISOString(), end.toISOString(), label);
		});

		$('#reservation-time').daterangepicker({
		  timePicker: true,
		  timePickerIncrement: 30,
		  locale: {
			format: 'MM/DD/YYYY h:mm A'
		  }
		});

	}
	  
	   
	   
	/* VALIDATOR */

	function init_validator () {

	  	if( typeof (validator) === 'undefined'){ return; }
		console.log('init_validator'); 
	  
		// initialize the validator function
		validator.message.date = 'not a real date';

		// validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
		$('form')
		.on('blur', 'input[required], input.optional, select.required', validator.checkField)
		.on('change', 'select.required', validator.checkField)
		.on('keypress', 'input[required][pattern]', validator.keypress);

		$('.multi.required').on('keyup blur', 'input', function() {
		validator.checkField.apply($(this).siblings().last()[0]);
		});

		$('form').submit(function(e) {
			e.preventDefault();
			var submit = true;

			// evaluate the form using generic validaing
			if (!validator.checkAll($(this))) {
			  submit = false;
			}

			if (submit)
			  this.submit();

			return false;
		});

	};
	   
	   
   /* CUSTOM NOTIFICATION */
		
	function init_CustomNotification() {
		
		console.log('run_customtabs');
		
		if( typeof (CustomTabs) === 'undefined'){ return; }
		console.log('init_CustomTabs');
		
		var cnt = 10;

		TabbedNotification = function(options) {
		  var message = "<div id='ntf" + cnt + "' class='text alert-" + options.type + "' style='display:none'><h2><i class='fa fa-bell'></i> " + options.title +
			"</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>" + options.text + "</p></div>";

		  if (!document.getElementById('custom_notifications')) {
			alert('doesnt exists');
		  } else {
			$('#custom_notifications ul.notifications').append("<li><a id='ntlink" + cnt + "' class='alert-" + options.type + "' href='#ntf" + cnt + "'><i class='fa fa-bell animated shake'></i></a></li>");
			$('#custom_notifications #notif-group').append(message);
			cnt++;
			CustomTabs(options);
		  }
		};

		CustomTabs = function(options) {
		  $('.tabbed_notifications > div').hide();
		  $('.tabbed_notifications > div:first-of-type').show();
		  $('#custom_notifications').removeClass('dsp_none');
		  $('.notifications a').click(function(e) {
			e.preventDefault();
			var $this = $(this),
			  tabbed_notifications = '#' + $this.parents('.notifications').data('tabbed_notifications'),
			  others = $this.closest('li').siblings().children('a'),
			  target = $this.attr('href');
			others.removeClass('active');
			$this.addClass('active');
			$(tabbed_notifications).children('div').hide();
			$(target).show();
		  });
		};

		CustomTabs();

		var tabid = idname = '';

		$(document).on('click', '.notification_close', function(e) {
		  idname = $(this).parent().parent().attr("id");
		  tabid = idname.substr(-2);
		  $('#ntf' + tabid).remove();
		  $('#ntlink' + tabid).parent().remove();
		  $('.notifications a').first().addClass('active');
		  $('#notif-group div').first().css('display', 'block');
		});
		
	};
   
	var grafikDuaObj;
	var grafikSatuObj;
	//var baseUrl = "http://localhost/motel/";
	var baseUrl = "http://monitoringp2tl.esy.es/";

	function init_charts() {
		
		console.log('run_charts  typeof [' + typeof (Chart) + ']');
	
		if( typeof (Chart) === 'undefined'){ return; }
		
		console.log('init_charts');
	
		
		Chart.defaults.global.legend = {
			enabled: false
		};
			
		// Grafik satu
		if ($('#grafikSatu').length ) {

			var filterGrafik1Thn = $("select[name=tahun]");
			var filterGrafik1User = $("select[name=id_user]");
			var filterGrafik1Bln = $("select[name=bulan]");

			var grafikSatuThnParam = getUrlParameter("tahun");
			var grafikSatuUserIdParam = getUrlParameter("id_user");
			var grafikSatuBlnParam = getUrlParameter("bulan");

			filterGrafik1Thn.val(grafikSatuThnParam);
			filterGrafik1User.val(grafikSatuUserIdParam);
			filterGrafik1Bln.val(grafikSatuBlnParam);
		  	
		  	console.log('filtering grafikSatu');
			console.log('param year: ' + grafikSatuThnParam);
			console.log('param id_user: ' + grafikSatuUserIdParam);
			console.log('param bulan: ' + grafikSatuBlnParam);

		  	  $.ajax({
				url: baseUrl + "admin/grafik/json_first_grafik",
				method: "GET",
				data: {
					tahun: grafikSatuThnParam,
					id_user: grafikSatuUserIdParam,
					bulan: grafikSatuBlnParam
				},
				success: function(data) {
					console.log(JSON.stringify(data));
					
					var hari = [];
					var presentase = [];
					var jmlPemeriksaan = [];

					for(var i in data) {
						hari.push(" " + data[i].hari);
						presentase.push(data[i].presentasi);
						jmlPemeriksaan.push(data[i].jml_pemeriksaan);
					}

					grafikSatuObj = new Chart($("#grafikSatu"), {
						type: 'bar',
						data: {
							labels: hari,
							datasets : [
								{
									label: 'Presentase (%)',
									backgroundColor: "#26B99A",
									data: presentase
								},
								{
									label: 'Temuan',
									backgroundColor: "#FFD91B",
									data: jmlPemeriksaan
								}
								
							]
						}
					});

					document.getElementById('js-legend-g1').innerHTML = grafikSatuObj.generateLegend();
					
				},
				error: function(data) {
					console.log(data);
				}
			});
		  
		}

		// Grafik dua

		if ($('#grafikDua').length ) {

			var filterGrafik2Tgl = $("#tgl_grafik2");

			var grafikDuaTglParam = getUrlParameter("tanggal");
			var tglFilterStr = grafikDuaTglParam.replace(/_/g, '-');
			var tglFilterDate = new Date(tglFilterStr);
			tglFilterStr = formattedDate(tglFilterDate);

			filterGrafik2Tgl.val(tglFilterStr);		
			
			console.log('filtering grafikDua');
			console.log('selected date: ' + grafikDuaTglParam);
			
		  	$.ajax({
				url: baseUrl + "admin/grafik/json_second_grafik",
				method: "GET",
				data: {
					tanggal: grafikDuaTglParam
				},
				success: function(data) {
					console.log(JSON.stringify(data));
					var golongan = [];
					var jmlPemeriksaan = [];
					var colors = [];

					for(var i in data) {
						golongan.push(" " + data[i].golongan);
						jmlPemeriksaan.push(data[i].jml_pemeriksaan);
						colors.push(data[i].color);
					}

					grafikDuaObj = new Chart($("#grafikDua"), {
						type: 'pie',
						data: {
							labels: golongan,
							datasets : [
								{
									label: golongan,
									backgroundColor: colors,
									data: jmlPemeriksaan
								}
							]
						}
					});

					document.getElementById('js-legend').innerHTML = grafikDuaObj.generateLegend(); 
				},
				error: function(data) {
					console.log(data);
				}
			});
		  
		}

		  
	}

	/* added by rmd */
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

	/*
		@params: d is object of Date(), -> d = new Date('2017-04-01');
	*/
	function formattedDate(d) {
	  let month = String(d.getMonth() + 1);
	  let day = String(d.getDate());
	  const year = String(d.getFullYear());

	  if (month.length < 2) month = '0' + month;
	  if (day.length < 2) day = '0' + day;

	  return `${day}/${month}/${year}`;
	}

	/* COMPOSE */
	
	function init_compose() {
	
		if( typeof ($.fn.slideToggle) === 'undefined'){ return; }
		console.log('init_compose');
	
		$('#compose, .compose-close').click(function(){
			$('.compose').slideToggle();
		});
	
	};
   
   	/* CALENDAR */
	  
    function  init_calendar() {
			
		if( typeof ($.fn.fullCalendar) === 'undefined'){ return; }
		console.log('init_calendar');
			
		var date = new Date(),
			d = date.getDate(),
			m = date.getMonth(),
			y = date.getFullYear(),
			started,
			categoryClass;

		var calendar = $('#calendar').fullCalendar({
		  header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listMonth'
		  },
		  selectable: true,
		  selectHelper: true,
		  select: function(start, end, allDay) {
			$('#fc_create').click();

			started = start;
			ended = end;

			$(".antosubmit").on("click", function() {
			  var title = $("#title").val();
			  if (end) {
				ended = end;
			  }

			  categoryClass = $("#event_type").val();

			  if (title) {
				calendar.fullCalendar('renderEvent', {
					title: title,
					start: started,
					end: end,
					allDay: allDay
				  },
				  true // make the event "stick"
				);
			  }

			  $('#title').val('');

			  calendar.fullCalendar('unselect');

			  $('.antoclose').click();

			  return false;
			});
		  },
		  eventClick: function(calEvent, jsEvent, view) {
			$('#fc_edit').click();
			$('#title2').val(calEvent.title);

			categoryClass = $("#event_type").val();

			$(".antosubmit2").on("click", function() {
			  calEvent.title = $("#title2").val();

			  calendar.fullCalendar('updateEvent', calEvent);
			  $('.antoclose2').click();
			});

			calendar.fullCalendar('unselect');
		  },
		  editable: true,
		  events: [{
			title: 'All Day Event',
			start: new Date(y, m, 1)
		  }, {
			title: 'Long Event',
			start: new Date(y, m, d - 5),
			end: new Date(y, m, d - 2)
		  }, {
			title: 'Meeting',
			start: new Date(y, m, d, 10, 30),
			allDay: false
		  }, {
			title: 'Lunch',
			start: new Date(y, m, d + 14, 12, 0),
			end: new Date(y, m, d, 14, 0),
			allDay: false
		  }, {
			title: 'Birthday Party',
			start: new Date(y, m, d + 1, 19, 0),
			end: new Date(y, m, d + 1, 22, 30),
			allDay: false
		  }, {
			title: 'Click for Google',
			start: new Date(y, m, 28),
			end: new Date(y, m, 29),
			url: 'http://google.com/'
		  }]
		});
		
	};
   
	/* DATA TABLES */
		
	function init_DataTables() {
		
		console.log('run_datatables');
		
		if( typeof ($.fn.DataTable) === 'undefined'){ return; }
		console.log('init_DataTables');
		
		var handleDataTableButtons = function() {
		  if ($("#datatable-buttons").length) {
			$("#datatable-buttons").DataTable({
			  dom: "Bfrtip",
			  buttons: [
				{
				  extend: "copy",
				  className: "btn-sm"
				},
				{
				  extend: "csv",
				  className: "btn-sm"
				},
				{
				  extend: "excel",
				  className: "btn-sm"
				},
				{
				  extend: "pdfHtml5",
				  className: "btn-sm"
				},
				{
				  extend: "print",
				  className: "btn-sm"
				},
			  ],
			  responsive: true
			});
		  }
		};

		TableManageButtons = function() {
		  "use strict";
		  return {
			init: function() {
			  handleDataTableButtons();
			}
		  };
		}();

		$('#datatable').dataTable();

		$('#datatable-keytable').DataTable({
		  keys: true
		});

		$('#datatable-responsive').DataTable();

		$('#datatable-scroller').DataTable({
		  ajax: "js/datatables/json/scroller-demo.json",
		  deferRender: true,
		  scrollY: 380,
		  scrollCollapse: true,
		  scroller: true
		});

		$('#datatable-fixed-header').DataTable({
		  fixedHeader: true
		});

		var $datatable = $('#datatable-checkbox');

		$datatable.dataTable({
		  'order': [[ 1, 'asc' ]],
		  'columnDefs': [
			{ orderable: false, targets: [0] }
		  ]
		});
		$datatable.on('draw.dt', function() {
		  $('checkbox input').iCheck({
			checkboxClass: 'icheckbox_flat-green'
		  });
		});

		TableManageButtons.init();
		
	};
	   
		
		
		

	function init_fancybox() {
		$('.fancybox').fancybox();
	}
	   
	/* main document ready */
	$(document).ready(function() {
		init_fancybox();	
		init_sidebar();
		init_daterangepicker();
		init_daterangepicker_right();
		init_daterangepicker_single_call();
		init_daterangepicker_reservation();
		init_charts();
		init_select2();
		init_validator();
		init_DataTables();
		init_calendar();
		init_compose();
		init_CustomNotification();
		

		$("small#ket_tgl").html('<i class="fa fa-calendar"></i> ' 
				+ ' ' + $("select[name=bulan]").find(':selected').text()
				+ ' ' + $("select[name=tahun]").val());

		var selecteTeam = $("select[name=id_user]");
		var ketTeam = (selecteTeam.val() != "") ? selecteTeam.find(':selected').text() : '-';
		$("small#ket_team").html(
			'<i class="fa fa-users"></i> '
			+ ketTeam
			);

		$("small#ket_tgl_grafik2").html('<i class="fa fa-calendar"></i> ' 
				+ $("#tgl_grafik2").val());

		
		// Handle grafik2 form submit (when filtering grafik2)
		$("#filter_grafik2").submit(function(e) {
			e.preventDefault();
			
			var inputFilterDate = $("#tgl_grafik2");
			var tglFilter = inputFilterDate.val();
			// Make it an array			
			tglFilter = tglFilter.split('/');

			// format it like you want :P
			tglFilter = tglFilter[2] + '_' + tglFilter[1] + '_' + tglFilter[0];

			window.open(baseUrl + 'admin/grafik/grafik2?tanggal=' + tglFilter, '_self');


		});

		// Set tanggal and team di halaman pemeriksaan
		var filterTglPemeriksaan = getUrlParameter('tanggal');
		var filterTeamPemeriksaan = getUrlParameter('id_user');
		$("#tgl_filter_pemeriksaan").val(filterTglPemeriksaan);
		$("#id_user_pemeriksaan").val(filterTeamPemeriksaan);

	});	
	

