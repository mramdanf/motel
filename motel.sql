-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2017 at 09:05 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `motel`
--

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemeriksaan`
--

CREATE TABLE `pemeriksaan` (
  `id` int(11) NOT NULL,
  `id_target_operasi` int(11) NOT NULL,
  `nama_pemeriksa` varchar(100) DEFAULT NULL,
  `golongan` varchar(10) DEFAULT NULL,
  `stand_kwh` varchar(100) NOT NULL,
  `error_kwh` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `photo` varchar(200) NOT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemeriksaan`
--

INSERT INTO `pemeriksaan` (`id`, `id_target_operasi`, `nama_pemeriksa`, `golongan`, `stand_kwh`, `error_kwh`, `keterangan`, `photo`, `tanggal`) VALUES
(5, 1, 'Asep', 'K4', '45000', '7800', 'Perangkat kurang baik', 'IMG_20170317_0609541.jpg', '2017-02-01'),
(6, 1, NULL, 'K4', '', '', '', '', '2017-02-01'),
(7, 1, NULL, 'P1', '', '', '', '', '2017-02-01'),
(8, 8, NULL, 'P1', '', '', '', '', '2017-02-01'),
(9, 16, NULL, 'K4', '', '', '', '', '2017-03-01'),
(10, 16, NULL, 'K4', '', '', '', '', '2017-03-01'),
(11, 16, NULL, 'K4', '', '', '', '', '2017-04-01'),
(12, 16, NULL, 'P1', '', '', '', '', '2017-04-01'),
(13, 16, NULL, 'P1', '', '', '', '', '2017-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `target_operasi`
--

CREATE TABLE `target_operasi` (
  `id` int(11) NOT NULL,
  `id_pelanggan` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `daya` varchar(100) DEFAULT NULL,
  `pembatas_daya` varchar(100) DEFAULT NULL,
  `ct_terpasang` varchar(100) DEFAULT NULL,
  `no_gardu` varchar(100) DEFAULT NULL,
  `merek` varchar(100) DEFAULT NULL,
  `seri` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `target_operasi`
--

INSERT INTO `target_operasi` (`id`, `id_pelanggan`, `id_user`, `nama`, `alamat`, `daya`, `pembatas_daya`, `ct_terpasang`, `no_gardu`, `merek`, `seri`, `tanggal`, `latitude`, `longitude`) VALUES
(1, '454333342226', 1, 'FIRDA', 'DN NGROWO            ', 'I2/82500', '3* 125', '150/5 A', 'T.', 'LANDIG & GYR', '215444747', '2017-02-01', -6.5429977, 106.773098),
(2, '231323243434', 1, 'ANI ', 'DN BLIMBING          ', 'B2/53000', '3 * 80', '100/5A', 'T.253', 'EDMI', '215449747', '2017-02-01', -6.5429977, 106.773098),
(3, '232424343434', 1, 'ANU', 'DN MADIOPURO         ', 'I2/82500', '3 * 125', '150/5 A', 'T.415', 'EDMI', '215444747', '2017-02-01', -6.5429977, 106.773098),
(4, '234545435455', 1, 'ADA', 'DN BELUK             ', 'I2/197000', '3*300', '300/5A', 'KUBIKEL.T.420', 'EDMI', '215544747', '2017-02-01', -6.5429977, 106.773098),
(5, '232445456566', 2, 'ADA', 'DN GROGOLAN JOMBANG', 'I2/197000', '3*300', '300/5A', 'T', 'EDMI', '211305940', '2017-02-01', -6.5429977, 106.773098),
(6, '343565676775', 2, 'ADA', 'DS TEMON', 'B2/105000', '3*160', '200/5A', 'KUBIKEL.T.420', 'EDMI', '215444747', '2017-02-01', -6.5429977, 106.773098),
(7, '897077898799', 2, 'ADA', 'DN SUBONTORO I       ', 'I2/197000', '3*300', '300/5A', 'KUBIKEL.T.420', 'EDMI', '215444747', '2017-02-10', -6.5429977, 106.773098),
(8, '564587900889', 2, 'ADA', 'DN TEMENGGUNGAN', 'I2/82500', '3*125', '150/5 A', 'KUBIKEL.T.420', 'EDMI', '215454747', '2017-03-01', -6.5429977, 106.773098),
(9, '564342343445', 2, 'ADA', 'JL GEDANGAN MOJOGENENG', 'I2/105000', '3*160', '200/5A', 'KUBIKEL.T.420', 'EDMI', '215444747', '2017-03-01', -6.5429977, 106.773098),
(10, '677687879886', 1, 'ADA', 'DN BEKUCUK TEMPURAN  ', 'B2/53000', '3*80', '100/5A', 'KUBIKEL.T.450', 'EDMI', '215444747', '2017-03-01', -6.5429977, 106.773098),
(11, '514500098493', 1, 'SOEHADI TANOYO', 'DN NGROWO', 'I2/82500', '3* 125', '150/5 A', 'T.', 'LANDIG & GYR', '215444747', '2017-03-01', -6.5429977, 106.773098),
(12, '514500336187', 1, 'POMP SMR AIR BRANTAS', 'DN BLIMBING', 'B2/53000', '3 * 80', '100/5A', 'T.253', 'EDMI', '215449747', '2017-03-01', -6.5429977, 106.773098),
(13, '514500441971', 1, 'PT.SARI BENIH UNGGUL     ', 'DN MADIOPURO         ', 'I2/82500', '3 * 125', '150/5 A', 'T.415', 'EDMI', '215444747', '2017-03-01', -6.5429977, 106.773098),
(14, '514500338988', 1, 'PT.KIMIA F. WT.DAKON WD20', 'DN BELUK             ', 'I2/197000', '3*300', '300/5A', 'KUBIKEL.T.420', 'EDMI', '215544747', '2017-03-01', -6.5429977, 106.773098),
(15, '514500842160', 1, 'FREDDY HARTONO', 'DN GROGOLAN JOMBANG', 'I2/197000', '3*300', '300/5A', 'T', 'EDMI', '211305940', '2017-03-01', -6.5429977, 106.773098),
(16, '514500682223', 1, 'H. SUYADI', 'DS TEMON', 'B2/105000', '3*160', '200/5A', 'KUBIKEL.T.420', 'EDMI', '215444747', '2017-04-01', -6.5429977, 106.773098),
(17, '514500516611', 1, 'H.MARGIONO               ', 'DN SUBONTORO I       ', 'I2/197000', '3*300', '300/5A', 'KUBIKEL.T.420', 'EDMI', '215444747', '2017-04-01', -6.5429977, 106.773098),
(18, '514500621093', 1, 'KAYAT, H', 'DN TEMENGGUNGAN', 'I2/82500', '3*125', '150/5 A', 'KUBIKEL.T.420', 'EDMI', '215454747', '2017-04-01', -6.5429977, 106.773098),
(19, '514500601237', 1, 'ABD KODIR H', 'JL GEDANGAN MOJOGENENG', 'I2/105000', '3*160', '200/5A', 'KUBIKEL.T.420', 'EDMI', '215444747', '2017-04-01', -6.5429977, 106.773098),
(20, '514500441764', 1, 'SUMUR YODIUM WD-9        ', 'DN BEKUCUK TEMPURAN  ', 'B2/53000', '3*80', '100/5A', 'KUBIKEL.T.450', 'EDMI', '215444747', '2017-04-01', -6.5429977, 106.773098),
(21, '514500441765', 1, 'ramdan', 'ramdan', 'B2/53001', '3*81', '100/5A', 'KUBIKEL.T.451', 'EDMI', '215444748', '2017-04-01', -6.5429977, 106.773098);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `hak_akses` varchar(50) DEFAULT NULL COMMENT '1:admin;selain 1:user biasa'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `password`, `no_hp`, `hak_akses`) VALUES
(1, 'Team 1', 'team1', '123', '082333444555', 'team1'),
(2, 'Team 2', 'team2', 'team2', '082333444555', 'team2'),
(3, 'Team 3', 'team3', 'team3', '082333444555', 'team3'),
(4, 'Team 4', 'team4', 'team4', '082333444555', 'team4'),
(5, 'admin', 'admin', 'admin', '08122233344', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `target_operasi`
--
ALTER TABLE `target_operasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `target_operasi`
--
ALTER TABLE `target_operasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
